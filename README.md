﻿# AutomatizacionQAElenas

Proyecto Web

Proyecto donde se realizan 3 escenarios de pruebas de la vista http://127.0.0.1:8000/polls/1/

#Para poder realizar la ejecución del proyecto se deben de tener en cuenta los siguientes pasos:

Para poder tener acceso a la url de pruebas

1. Clonar el proyecto backend y frontend del repositorio https://gitlab.com/brasilikum/pruebaqa
2. Correr el proyecto en Visual Studio Code o PyCharm
3. Realizar la creación del entorno virtual python3 -m venv .venv
4. Activar el entorno 
5. Instalar las dependencias pip3 install -r requirements.txt
6. Correr el servidor python manage.py runserver


#Para realizar la ejecución de las pruebas los requerimientos son los siguientes:

1. Java JDK 1.8 (Con variables de entorno configuradas)
2. Gradle versión 4.7 o superior (Con variables de entorno configuradas)
3. Cucumber
4. Eclipse IDE o Intellij IDE

# Instalación:

- Para clonar este repositorio localmente, se debe ejecutar el siguiente comando: 

 Gitlab
git clone https://gitlab.com/Estefa/automatizacionqaelenas.git
Azure Devops
git clone https://estefaniaperez75600@dev.azure.com/estefaniaperez75600/PruebaElena/_git/AutomatizacionElenas
	
- Importar el proyecto desde Eclipse o IntelliJ IDE bajo la estructura de un proyecto Gradle existente 


#Para correr el proyecto
Ejecutar el comando:

gradle clean build test -i

# Navegadores Web:

La automatización actualmente se ejecuta en los siguientes navegadores:
- Google Chrome v91

# Notas:

# Detalles generales de la implementación:

La estructura del proyecto está basada en el patron screenplay. Cada línea de los escenarios de pruebas creados en el feature bajo lenguaje Gherkin, se conectan 
con un método de las clases StepDefinitions con la ayuda de anotaciones @Dado, @Cuando y @Entonces, desde el StepDefinitions se conectan con las clases tipo Task 
donde se realizan las acciones de alto nivel.

La estructura completa del proyecto es la siguiente:
ProyectoElenas
│
src
├───main
│   ├───java
│   │   └───co
│   │       └───com
│   │           └───certificacion
│   │               └───elenas
│   │                   └───├───exceptions
│   │                       ├───questions
│   │                       ├───tasks
│   │ 			    ├───models 
│   │                       └───userinterfaces
│   └───resources
└───test
    ├───java
    │   └───co
    │       └───com
    │           └───certificacion
    │               └───elenas
    │                   └───
    │                       ├───runners
    │                       └───stepdefinitions
    └───resources
        └───features
Descripción de los paquetes:

..ProyectoElenas\src\main\java\co\com\certificacion\elenas
	*\questions: Clases con las que se obtienen valores para luego ser verificadas en los stepdefinitions (asserts). 
	*\tasks: Clases que realizan las acciones de alto nivel, como lo es ingresar datos de un formulario, realizar votos etc. 
	*\userinterfaces: Clases donde se mapean los elementos de la interfaz de usuario y a su vez para la interacción con cada uno de estos elementos.
	*\exceptions: Clase para el manejo de los errores 
	*\models: Clases relacionadas con el modelo de dominio

..ProyectoElenas\src\test\java\co\com\certificacion\elenas
	*\runners: Clases para ejecutar la automatización con los escenarios indicados en el feature.
	*\definitions: Clases que son el punto de entrada del feature para traducir de lenguaje natural a lenguaje máquina y así permitir la ejecución de la automatización. 

..ProyectoElenas\src\test\resources\features
	Se encuentra los features del proyecto escritos en lenguaje gherkin.

# Herramientas utilizadas durante el desarrollo del proyecto:

La automatización fue desarrollada con:
 - Screenplay
 - Gradle - Manejador de dependencias
 - Selenium Web Driver - Herramienta para  automatizar acciones en navegadores web
 - Cucumber - Framework para automatizar pruebas BDD
 - Serenity BDD - Biblioteca de código abierto para la generación de reportes
 - Gherkin - Lenguaje Business Readable DSL (Lenguaje especifico de dominio legible por el negocio)

# Versionamiento:

Se usa GIT para el control de versiones.


Proyecto Mobile App El Empleo.com

#Para realizar la ejecución de las pruebas los requerimientos son los siguientes:

1. Java JDK 1.8 (Con variables de entorno configuradas)
2. Gradle versión 4.7 o superior (Con variables de entorno configuradas)
3. Cucumber
4. Eclipse IDE o Intellij IDE
5. Appium Server version 1.7.2 o superior
6. Un emulador o dispositivo de android corriendo

Configurar serenity.properties

Se debe configurar las siguientes propiedades:

appium.hub Debe coincidir con la ip y puerto donde va a correr el appium server

appium.platformVersion Version de android del dispositivo emulador que vamos a utilizar

appium.app Ubicación de la apk

appium.deviceName Nombre del emulador o dispositivo que vas a utilizar, para esto puedes utilizar el adb

adb devices
Para que el anterior comando funcione adb debe estar como variable de entorno

# Instalación:

- Para clonar este repositorio localmente, se debe ejecutar el siguiente comando: 

 Gitlab
git clone https://gitlab.com/Estefa/automatizacionqaelenas.git
Azure Devops
git clone https://estefaniaperez75600@dev.azure.com/estefaniaperez75600/PruebaElena/_git/AutomatizacionElenas
	
- Importar el proyecto desde Eclipse o IntelliJ IDE bajo la estructura de un proyecto Gradle existente 

# Detalles generales de la implementación:

La estructura del proyecto está basada en el patron screenplay. Cada línea de los escenarios de pruebas creados en el feature bajo lenguaje Gherkin, se conectan 
con un método de las clases StepDefinitions con la ayuda de anotaciones @Dado, @Cuando y @Entonces, desde el StepDefinitions se conectan con las clases tipo Task 
donde se realizan las acciones de alto nivel.

La estructura completa del proyecto es la siguiente:
AppEmpleo
│
src
├───main
│   ├───java
│   │   └───co
│   │       └───com
│   │           └───certificacion
│   │               └───app
│   │  			 └───empleo
│   │                        ├───questions
│   │                        ├───tasks
│   │ 			     ├───models 
│   │                        └───userinterfaces
│   └───resources
└───test
    ├───java
    │   └───co
    │       └───com
    │           └───certificacion
    │               └───app
    │                   └───empleo
    │                       ├───runners
    │                       └───stepdefinitions
    └───resources
	└───app
        └───features
Descripción de los paquetes:

..AppEmpleo\src\main\java\co\com\certificacion\app\empleo
	*\questions: Clases con las que se obtienen valores para luego ser verificadas en los stepdefinitions (asserts). 
	*\tasks: Clases que realizan las acciones de alto nivel, como lo es realizar login y crear alertas.
	*\userinterfaces: Clases donde se mapean los elementos de la interfaz de usuario y a su vez para la interacción con cada uno de estos elementos.
	*\models: Clases relacionadas con el modelo de dominio

..AppEmpleo\src\test\java\co\com\certificacion\app\empleo
	*\runners: Clases para ejecutar la automatización con los escenarios indicados en el feature.
	*\definitions: Clases que son el punto de entrada del feature para traducir de lenguaje natural a lenguaje máquina y así permitir la ejecución de la automatización. 

..AppEmpleo\src\test\resources\features
	Se encuentra los features del proyecto escritos en lenguaje gherkin.

..AppEmpleo\src\test\resources\app
	Donde se encuentra el apk de la aplicación


En el repositorio de azure devops se encuentra la creación del pipeline CI/CD

# Autor:

*Estefania Perez Garzon


