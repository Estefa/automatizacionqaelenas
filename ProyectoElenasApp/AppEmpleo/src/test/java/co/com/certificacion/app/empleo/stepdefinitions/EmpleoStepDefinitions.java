package co.com.certificacion.app.empleo.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

import co.com.certificacion.app.empleo.models.Formulario;
import co.com.certificacion.app.empleo.questions.AlertaEmpleoExitosa;
import co.com.certificacion.app.empleo.questions.LoginExitoso;
import co.com.certificacion.app.empleo.tasks.CrearAlertaEmpleo;
import co.com.certificacion.app.empleo.tasks.LoguearseFormulario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;


import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class EmpleoStepDefinitions {

    @Before
    public void prepareStage() {
        OnStage.setTheStage(new OnlineCast());
    }


    @When("^el cliente quiere loguearse en la aplicacion del empleo.com e ingresa los datos solicitados$")
    public void elClienteQuiereLoguearseEnLaAplicacionDelEmpleoEingresaLosDatosSolicitados(List<Formulario> formulario)  {
        theActorCalled("Estefa").attemptsTo(LoguearseFormulario.enLaApp(formulario.get(0)));
    }

    @When("^crea una alerta de empleo$")
    public void creaUnaAlertaDeEmpleo(List<Formulario> formulario)  {
        theActorCalled("Estefa").attemptsTo(CrearAlertaEmpleo.enLaApp(formulario.get(0).getTituloEmpleo(),
                formulario.get(0).getProfesion(),
                formulario.get(0).getSalario()));
    }


    @Then("^el usuario puede validar el login exitoso$")
    public void elUsuarioPuedeValidarElLoginExitoso(){
        theActorInTheSpotlight().should(seeThat(LoginExitoso.esVisible()));
    }

    @Then("^el usuario puede validar (.*) en alertas$")
    public void elUsuarioPuedeValidarElTituloDelEmpleoEnAlertas(String mensaje){
        theActorInTheSpotlight().should(seeThat(AlertaEmpleoExitosa.enAppEmpleo()
                ,containsString(mensaje)));
    }

}
