package co.com.certificacion.app.empleo.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/app_empleo.feature",
        glue = "co.com.certificacion.app.empleo.stepdefinitions",
        snippets = SnippetType.CAMELCASE)
public class AppEmpleo {

}
