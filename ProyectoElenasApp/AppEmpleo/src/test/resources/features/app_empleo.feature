#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#														          MODULO VOTACION
#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#					Authors:
#					estefapg25@gmail.com
#
#         language: es
#
#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Característica: App el empleo.com
  Yo como Usuario
  Quiero acceder y buscar un empleo



  @LoginEmpleo
  Esquema del escenario: Validar que un usuario pueda loguearse en la APP del empleo
    Cuando el cliente quiere loguearse en la aplicacion del empleo.com e ingresa los datos solicitados
      | correo | contrasena|
      |<correo>| <contrasena>|
    Entonces el usuario puede validar el login exitoso

    Ejemplos:
      | correo | contrasena|
      | prueba124estefa@gmail.com |987654321test.|



  @CrearAlertaEmpleo
  Esquema del escenario: Validar que un usuario pueda loguearse en la APP del empleo y crear una alerta de empleo
    Cuando el cliente quiere loguearse en la aplicacion del empleo.com e ingresa los datos solicitados
      | correo | contrasena|
      |<correo>| <contrasena>|
    Y crea una alerta de empleo
    |tituloEmpleo|profesion|salario|
    |<tituloEmpleo>|<profesion>|<salario>|
    Entonces el usuario puede validar alertas creadas en alertas

    Ejemplos:
      | correo | contrasena|tituloEmpleo|profesion|salario|
      | prueba124estefa@gmail.com |987654321test.|EmpleosAnalista|Analista|4|
