package co.com.certificacion.app.empleo.questions;

import co.com.certificacion.app.empleo.userinterfaces.AlertasEmpleo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class AlertaEmpleoExitosa implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(AlertasEmpleo.RESULTADO_ALERTAS).viewedBy(actor).asString();
    }

    public static AlertaEmpleoExitosa enAppEmpleo(){
        return new AlertaEmpleoExitosa();
    }

}
