package co.com.certificacion.app.empleo.tasks;

import co.com.certificacion.app.empleo.models.Formulario;
import co.com.certificacion.app.empleo.userinterfaces.LogueoEmpleo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LoguearseFormulario implements Task {

    private Formulario formulario;

    public LoguearseFormulario(Formulario formulario) {
        this.formulario = formulario;
    }

    public static LoguearseFormulario enLaApp(Formulario formulario){
        return instrumented(LoguearseFormulario.class, formulario);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(formulario.getCorreo()).into(LogueoEmpleo.CORREO),
                Enter.theValue(formulario.getContrasena()).into(LogueoEmpleo.CONTRASENA),
                Click.on(LogueoEmpleo.BOTON_LOGIN));
    }
}
