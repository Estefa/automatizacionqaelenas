package co.com.certificacion.app.empleo.models;


public class Formulario {

    private final String correo;
    private final String contrasena;
    private final String tituloEmpleo;
    private final String profesion;
    private final String salario;

    public Formulario(String correo, String contrasena, String tituloEmpleo, String profesion, String salario) {
        this.correo = correo;
        this.contrasena = contrasena;
        this.tituloEmpleo = tituloEmpleo;
        this.profesion = profesion;
        this.salario = salario;

    }

    public String getCorreo() {
        return correo;
    }


    public String getContrasena() {
        return contrasena;
    }


    public String getTituloEmpleo() {
        return tituloEmpleo;
    }

    public String getProfesion() {
        return profesion;
    }

    public String getSalario() {
        return salario;
    }
}
