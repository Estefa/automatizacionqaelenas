package co.com.certificacion.app.empleo.tasks;

import co.com.certificacion.app.empleo.models.Formulario;
import co.com.certificacion.app.empleo.userinterfaces.AlertasEmpleo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.*;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class CrearAlertaEmpleo implements Task {

        private String tituloEmpleo;
        private String profesion;
        private String salario;

        public CrearAlertaEmpleo(String tituloEmpleo, String profesion, String salario) {
            this.profesion = profesion;
            this.salario = salario;
            this.tituloEmpleo = tituloEmpleo;
        }

        public static CrearAlertaEmpleo enLaApp(String tituloEmpleo, String salario, String profesion){
            return instrumented(CrearAlertaEmpleo.class, tituloEmpleo, salario, profesion);
        }

        @Override
        public <T extends Actor> void performAs(T actor) {
            actor.attemptsTo(
                    Click.on(AlertasEmpleo.ICONO_ALERTAS),
                    Click.on(AlertasEmpleo.CREAR_ALERTAS),
                    Enter.theValue(tituloEmpleo).into(AlertasEmpleo.TITULO_ALERTAS),
                    Enter.theValue(profesion).into(AlertasEmpleo.PALABRA_CLAVE),
                    Click.on(AlertasEmpleo.SALARIO),
                    Click.on(AlertasEmpleo.GUARDAR));
        }
}




