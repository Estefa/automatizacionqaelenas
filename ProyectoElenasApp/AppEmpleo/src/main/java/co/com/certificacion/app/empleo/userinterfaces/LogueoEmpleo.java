package co.com.certificacion.app.empleo.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LogueoEmpleo {

    public final static Target CORREO = Target.the("Campo de texto para ingresar el correo")
            .located(By.id("com.elempleo.app:id/login_et_email"));

    public final static Target CONTRASENA= Target.the("Campo de texto para ingresar la contraseña")
            .located(By.id("com.elempleo.app:id/login_et_password"));

    public final static Target MENSAJE_LOGIN=Target.the("Mensaje despues de loguearse de forma exitosa")
            .located(By.id("com.elempleo.app:id/home_search_title"));

    public final static Target BOTON_LOGIN= Target.the("Botón para loguearse")
            .located(By.id("com.elempleo.app:id/login_bt_ingresar"));

}

