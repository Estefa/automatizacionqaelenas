package co.com.certificacion.app.empleo.questions;

import co.com.certificacion.app.empleo.userinterfaces.LogueoEmpleo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

public class LoginExitoso implements Question<Boolean> {

    @Override
    public Boolean answeredBy(Actor actor) {
        return LogueoEmpleo.MENSAJE_LOGIN.resolveFor(actor).isVisible();
    }

    public static LoginExitoso esVisible() {
        return new LoginExitoso();
    }
}

