
package co.com.certificacion.app.empleo.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class AlertasEmpleo {

    public final static Target ICONO_ALERTAS = Target.the("Icono para ir a crear alertas")
            .located(By.id("com.elempleo.app:id/my_account_box_noti"));

    public final static Target CREAR_ALERTAS = Target.the("Dar click a crear alertas")
            .located(By.id("com.elempleo.app:id/fab_add"));

    public final static Target TITULO_ALERTAS = Target.the("Cuadro de texto para ingresar el titulo de la alerta de empleo")
            .located(By.xpath("//android.widget.EditText[@text='Titulo de la alerta']"));


    public final static Target PALABRA_CLAVE = Target.the("Cuadro de texto para la palabra clave")
            .locatedBy("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.EditText");

    public final static Target SALARIO = Target.the("label de los resultados de la busqueda")
            .locatedBy("//android.widget.CheckBox[8]");

    public final static Target GUARDAR = Target.the("Botón para guardar alerta")
            .located(By.id("com.elempleo.app:id/save_button"));

    public final static Target RESULTADO_ALERTAS = Target.the("texto con la alerta creada")
            .located(By.id("com.elempleo.app:id/frequency_help_tv"));



}
