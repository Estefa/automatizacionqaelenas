#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#														          MODULO VOTACION
#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
#					Authors:
#					estefapg25@gmail.com
#
#         language: es
#
#
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------#
Característica: Modulo conteo de votaciones
  Yo como Usuario
  Quiero acceder y poder votar, elegir la categoria que desee y totalizar las votaciones

  @VotacionCategoria
  Esquema del escenario: Validar que un usuario pueda votar por una categoria
    Dado el cliente ingresa al modulo de votacion
    Cuando el cliente selecciona la opcion por la que desea votar
      | <categoria> |
    Entonces el usuario puede validar en la url results de los resultados

    Ejemplos:
      | categoria |
      | Not much  |
      | The sky   |
      | La la la  |

  @MensajePorNoSeleccionCategoria
  Escenario: Validar que si un usuario no selecciona alguna opción para votar se presente un error
    Dado el cliente ingresa al modulo de votacion
    Cuando el cliente no selecciona opcion
    Entonces Al usuario se le presenta el mensaje de error You didn't select a choice.



  @SumaDeVotosCategorias
  Esquema del escenario: Validar que los votos si se vayan acumulando y lo que se muestra en la página sea correcto
    Dado el cliente ingresa al modulo de votacion
    Cuando el cliente realiza el voto en la pagina de la categoria
      | <categoria> |
    Entonces Al usuario se le presenta la cantidad de votos totales acumuladas
      | <categoria> |

    Ejemplos:
    |categoria |
    |The sky |
