package co.com.certificacion.elenas.stepdefinitions;

import co.com.certificacion.elenas.exceptions.ElementoNoVisibleException;
import co.com.certificacion.elenas.models.Datos;
import co.com.certificacion.elenas.questions.ValidarAcumuladoVotos;
import co.com.certificacion.elenas.questions.ValidarMensajeError;
import co.com.certificacion.elenas.questions.ValidarUrlVotacion;
import co.com.certificacion.elenas.tasks.ElegirCategoria;
import co.com.certificacion.elenas.tasks.NoSeleccionarVoto;
import co.com.certificacion.elenas.tasks.Votar;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

public class VotacionesStepDefinitions {


    Actor user = Actor.named("usuario");

    @Managed(driver = "chrome")
    WebDriver driver;

    @Before
    public void config() {
        user.can(BrowseTheWeb.with(driver));
    }

    @Dado("^el cliente ingresa al modulo de votacion$")
    public void elClienteIngresaAlModuloDeVotacion() {
        user.wasAbleTo(Open.url("http://127.0.0.1:8000/polls/1/"));

    }

    @Cuando("^el cliente selecciona la opcion por la que desea votar$")
    public void elClienteSeleccionaLaOpcionPorLaQueDeseaVotar(List<Datos> opcion) {
        user.attemptsTo(ElegirCategoria.deLaVotacion(opcion.get(0).getCategoria()));

    }

    @Cuando("^el cliente no selecciona opcion$")
    public void elClienteNoSeleccionaOpcion() {
    user.attemptsTo(NoSeleccionarVoto.paraVotacion());
    }

    @Cuando("^el cliente realiza el voto en la pagina de la categoria$")
    public void elClienteRealizaElVotoEnLaPaginaDeLaCategoria(List<Datos> opcion) {
        user.attemptsTo(Votar.enLaEncuesta(opcion.get(0).getCategoria()));
    }



    @Entonces("^Al usuario se le presenta el mensaje de error (.*)$")
    public void alUsuarioSeLePresentaElMensajeDeError(String mensaje) {
        user.should(seeThat(ValidarMensajeError.enlaVotacion(), containsString(mensaje)).orComplainWith(ElementoNoVisibleException.class,
                ElementoNoVisibleException.MSG_ERROR));

    }


    @Entonces("^el usuario puede validar en la url (.*) de los resultados$")
    public void elUsuarioPuedeValidarEnLaUrlDeLosResultados(String mensaje) {

        user.should(seeThat(ValidarUrlVotacion.enElNavegador(), containsString(mensaje)).orComplainWith(ElementoNoVisibleException.class,
                ElementoNoVisibleException.URL));

    }

    @Entonces("^Al usuario se le presenta la cantidad de votos totales acumuladas$")
    public void alUsuarioSeLePresentaLaCantidadDeVotosTotalesAcumuladas(List<Datos> opcion) {

        user.should(seeThat(ValidarAcumuladoVotos.enLaEncuesta(opcion.get(0).getCategoria())).orComplainWith(ElementoNoVisibleException.class,
                ElementoNoVisibleException.CANTIDAD_VOTOS));

    }

}
