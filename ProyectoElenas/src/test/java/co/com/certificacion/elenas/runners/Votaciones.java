package co.com.certificacion.elenas.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/votacione2e.feature",
        glue = "co.com.certificacion.elenas.stepdefinitions",
        snippets = SnippetType.CAMELCASE)

public class Votaciones {
}
