package co.com.certificacion.elenas.exceptions;

public class ElementoNoVisibleException extends AssertionError {

    public static final String MSG_ERROR = "El mensaje de error no se visualizó en la pantalla";

    public static final String CANTIDAD_VOTOS = "La cantidad de votos para la categoria La la la no se visualizó en la pantalla";

    public static final String URL= "La url esperada no se visualizó en la pantalla";



    public ElementoNoVisibleException(String message) {
        super(message);
    }

    public ElementoNoVisibleException(String message, Throwable cause) {
        super(message, cause);
    }
}
