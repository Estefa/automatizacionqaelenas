package co.com.certificacion.elenas.tasks;

import co.com.certificacion.elenas.userinterfaces.PantallaCategorias;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.Text;

import javax.swing.*;


public class Votar implements Task {

    private static Double sumaVotos;
    private String opcion;
    String votoActual, votoActualSubString;


    public Votar(String opcion) {
        this.opcion = opcion;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {


        switch (opcion) {
            case "Not much":
                actor.attemptsTo(Click.on(PantallaCategorias.CATEGORIA_NOT_MUCH), Click.on(PantallaCategorias.VOTAR));
                votoActual = Text.of(PantallaCategorias.CANTIDAD_VOTOS_ACTUALES_CATEGORIA_UNO).viewedBy(actor).asString();
                actor.attemptsTo(Click.on(PantallaCategorias.VOTAR_OTRA_VEZ), Click.on(PantallaCategorias.CATEGORIA_NOT_MUCH), Click.on(PantallaCategorias.VOTAR));
                break;
            case "The sky":
                actor.attemptsTo(Click.on(PantallaCategorias.CATEGORIA_THE_SKY), Click.on(PantallaCategorias.VOTAR));
                votoActual = Text.of(PantallaCategorias.CANTIDAD_VOTOS_ACTUALES_CATEGORIA_DOS).viewedBy(actor).asString();
                actor.attemptsTo(Click.on(PantallaCategorias.VOTAR_OTRA_VEZ), Click.on(PantallaCategorias.CATEGORIA_THE_SKY), Click.on(PantallaCategorias.VOTAR));
                break;
            case "La la la":
                actor.attemptsTo(Click.on(PantallaCategorias.CATEGORIA_LA_LA_LA), Click.on(PantallaCategorias.VOTAR));
                votoActual = Text.of(PantallaCategorias.CANTIDAD_VOTOS_ACTUALES_CATEGORIA_TRES).viewedBy(actor).asString();
                actor.attemptsTo(Click.on(PantallaCategorias.VOTAR_OTRA_VEZ), Click.on(PantallaCategorias.CATEGORIA_LA_LA_LA), Click.on(PantallaCategorias.VOTAR));
                break;

            default:
                JOptionPane.showMessageDialog(null, "Selecciona una opcion valida");
                break;
        }

            votoActualSubString = votoActual.replaceAll("\\D+", "");
            double votoActualDouble = Double.parseDouble(votoActualSubString);
            sumaVotos = votoActualDouble + 1;


    }


    public static Double getSumaVotos() {
        return sumaVotos;
    }

    public static Votar enLaEncuesta(String opcion) {

        return new Votar(opcion);
    }

}
