package co.com.certificacion.elenas.tasks;

import co.com.certificacion.elenas.userinterfaces.PantallaCategorias;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

public class NoSeleccionarVoto implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(Click.on(PantallaCategorias.VOTAR));
    }

    public static NoSeleccionarVoto paraVotacion() {
        return new NoSeleccionarVoto();
    }



}
