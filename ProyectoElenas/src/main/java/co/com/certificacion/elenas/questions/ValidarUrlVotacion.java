package co.com.certificacion.elenas.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidarUrlVotacion implements Question<String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidarUrlVotacion.class);


    @Override
    public String answeredBy(Actor actor) {

        String urlActual = BrowseTheWeb.as(actor).getDriver().getCurrentUrl();

        LOGGER.info("URL: " + urlActual);

        return urlActual;

    }


        public static ValidarUrlVotacion enElNavegador() {

            return new ValidarUrlVotacion();
        }

}


