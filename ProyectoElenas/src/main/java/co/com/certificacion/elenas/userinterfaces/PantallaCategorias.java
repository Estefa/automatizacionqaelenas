package co.com.certificacion.elenas.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;



public class PantallaCategorias {

    public static final Target CATEGORIA_NOT_MUCH = Target.the("Radio button para seleccionar categoria not much")
            .located(By.id("choice1"));
    public static final Target CATEGORIA_LA_LA_LA = Target.the("Radio button para seleccionar categoria la la la")
            .located(By.id("choice3"));
    public static final Target CATEGORIA_THE_SKY = Target.the("Radio button para seleccionar categoria the sky")
            .located(By.id("choice2"));
    public static final Target VOTAR = Target.the("Botón para realizar voto")
            .locatedBy("//input[@value='Vote']");
    public static final Target MSG_VOTACION = Target.the("Mensaje votacion exitosa")
            .locatedBy("//a[contains(text(),'Vote again')]");

    public static final Target MSG_ERROR = Target.the("Mensaje de error en la votación")
            .locatedBy("//strong[contains(text(),'You')]");
    public static final Target CANTIDAD_VOTOS_ACTUALES_CATEGORIA_TRES= Target.the("Label con la cantidad de votos acumuladas para La la la")
            .locatedBy("//Li[contains(text(),'La')]");
    public static final Target CANTIDAD_VOTOS_ACTUALES_CATEGORIA_UNO= Target.the("Label con la cantidad de votos acumuladas para Not much")
            .locatedBy("//Li[contains(text(),'Not')]");
    public static final Target CANTIDAD_VOTOS_ACTUALES_CATEGORIA_DOS= Target.the("Label con la cantidad de votos acumuladas para The Sky")
            .locatedBy("//Li[contains(text(),'The')]");
    public static final Target VOTAR_OTRA_VEZ = Target.the("Link para votar de nuevo").
            locatedBy("//a[contains(text(),'Vote')]");
    public static final Target VOTOS_LA_LA_LA = Target.the("Label con la cantidad de votos de La la la")
            .locatedBy("//Li[contains(text(),'La')]");
    public static final Target VOTOS_NOT_MUCH = Target.the("Label con la cantidad de votos de La la la")
            .locatedBy("//Li[contains(text(),'Not')]");
    public static final Target VOTOS_THE_SKY = Target.the("Label con la cantidad de votos de La la la")
            .locatedBy("//Li[contains(text(),'The')]");



}
