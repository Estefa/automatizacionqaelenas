package co.com.certificacion.elenas.questions;

import co.com.certificacion.elenas.userinterfaces.PantallaCategorias;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class ValidarMensajeError  implements Question<String> {

    public static ValidarMensajeError enlaVotacion() {

        return new ValidarMensajeError();
    }

    @Override
    public String answeredBy(Actor actor) {
        return Text.of(PantallaCategorias.MSG_ERROR).viewedBy(actor).asString();
    }
}
