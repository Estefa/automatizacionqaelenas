package co.com.certificacion.elenas.tasks;

import co.com.certificacion.elenas.models.Datos;
import co.com.certificacion.elenas.userinterfaces.PantallaCategorias;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public class ElegirCategoria implements Task {

    private String opcion;

    public ElegirCategoria(String opcion) {

        this.opcion = opcion;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        switch(opcion) {
            case "Not much":
                actor.attemptsTo(Click.on(PantallaCategorias.CATEGORIA_NOT_MUCH));
                break;
            case "The sky":
                actor.attemptsTo(Click.on(PantallaCategorias.CATEGORIA_THE_SKY));
                break;
            case "La la la":
                actor.attemptsTo(Click.on(PantallaCategorias.CATEGORIA_LA_LA_LA));
                break;

            default:
                    JOptionPane.showMessageDialog(null,"Selecciona una opcion valida");
                    break;
        }

        actor.attemptsTo(Click.on(PantallaCategorias.VOTAR));
    }

    public static ElegirCategoria deLaVotacion(String opcion)
    {
        return new ElegirCategoria(opcion);
    }

}


