package co.com.certificacion.elenas.models;

public class Datos {

    private String categoria;

    public Datos(String categoria) {
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
