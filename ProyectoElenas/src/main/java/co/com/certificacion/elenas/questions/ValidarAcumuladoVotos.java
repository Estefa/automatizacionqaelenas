package co.com.certificacion.elenas.questions;


import co.com.certificacion.elenas.tasks.Votar;
import co.com.certificacion.elenas.userinterfaces.PantallaCategorias;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import javax.swing.*;

public class ValidarAcumuladoVotos implements Question<Boolean> {

    private String opcion;
    String resultVotos;


    public ValidarAcumuladoVotos(String opcion) {
        this.opcion = opcion;
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        switch (opcion) {
            case "Not much":
                resultVotos = Text.of(PantallaCategorias.VOTOS_NOT_MUCH).viewedBy(actor).asString();
                break;

            case "The sky":
                resultVotos = Text.of(PantallaCategorias.VOTOS_THE_SKY).viewedBy(actor).asString();
                break;

            case "La la la":
                resultVotos = Text.of(PantallaCategorias.VOTOS_LA_LA_LA).viewedBy(actor).asString();
                break;
            default:
                JOptionPane.showMessageDialog(null, "Selecciona una opcion valida");
                break;
        }

        resultVotos = resultVotos.replaceAll("\\D+","");
        Double resultVotosDouble = Double.parseDouble(resultVotos);

        double votoCalculo = Votar.getSumaVotos();

        if (resultVotosDouble.equals(votoCalculo)) {
            return true;
        } else
            return false;
    }

    public static ValidarAcumuladoVotos enLaEncuesta(String opcion) {

        return new ValidarAcumuladoVotos(opcion);
    }
}
